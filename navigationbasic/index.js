/**
 * @format
 */

// import {AppRegistry} from 'react-native';
import App from './App';
import App2 from './App2';
import SideMenu from './SideMenu'
import WebPay from './WebPay'
// import {name as appName} from './app.json';
import {Navigation} from "react-native-navigation";


// AppRegistry.registerComponent(appName, () => App);

Navigation.registerComponent('App1', () => App);
Navigation.registerComponent('App2', () => App2);
Navigation.registerComponent('SideMenu', () => SideMenu);
Navigation.registerComponent('WebPay', () => WebPay);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            sideMenu: {
                id: "sideMenu",
                left: {
                    component: {
                        id: "Drawer",
                        name: "SideMenu"
                    },
                    visible: true
                },
                center: {
                    stack: {
                        id: "AppRoot",
                        children: [{
                            component: {
                                id: "App",
                                name: "WebPay"
                            }
                        }]
                    }
                }
            }
        }
    });
});